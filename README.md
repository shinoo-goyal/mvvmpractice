# MVVMPractice

## Synopsis

The goal of this project is to create a sample app in MVVM architecture using ViewModel and LiveData architecture components of Android. This app will helps us understand the basic flow in MVVM architecture.The app includes following features:

- Accessing StarWars API
- Showing list of StarWars characters
- Search StarWars characters by name

## Build variants:

The supported build types are:

- **debug** for development and testing the app.
- **omqa:** The main testing effort will be spent on this environment
- **release:** Not used at this moment as it is for client release

## Application architecture:
The app is structured following the Clean Architecture architectural pattern.

The UI, presentation logic, business logic and data management logic are clearly separated in layers. Each layer is allowed to communicate only with the layer above and the layer below. All the layers communicate by exchanging the same Model objects, which are simple POJOs representing the involved real-world objects.

The project is organised in three layers: Data, Domain and Presentation.

The Domain layer contains all of the classes that represent the real life objects managed by the app, classified as Models. These objects are passed between layers during internal communication. All of the models in this layer are POJOs. They are completely decoupled from the Android framework and have no responsibilities besides being a data representation.

The Data layer contains all of the objects responsible for storing and retrieving data used by the app. This includes: service APIs and local storage. All of the objects that provide data implement the relevant Repository interfaces provided by the Domain layer.

The Presentation layer contains UI and Presentation logic, and it's organised following the Model-View-ViewModel (MVVM) pattern.

The app makes use of multiple design patterns including:

- Dependency Inversion pattern in order to achieve low coupling between the components and to simplify testing.
- Model-View-ViewModel pattern to achieve separation between presentation logic and domain logic.

##Configuration
App has different configuration files for different environments hence different flavours can easily use their own desired configurations.

## Testing:
We are using Mockito and Junit. We have written test for ViewModel, Manger and Mapper. We will also write test cases for Usecase if it is complex.

## Dependencies:
See app/build.gradle file for complete list of dependencies.

## Jenkins:

Please go to Jenkins for getting latest build for QA.The app is configured to use HockeyApp.The final build gets uploaded on the HockeyApp.

Here is a brief overview though of the jobs currently setup:

- [Dev](https://ci.omdev.io/job/Practice-MVVM-Android-Dev/) - Run tests as soon as code is pushed to develop.
- [OMQA](https://ci.omdev.io/job/Practice-MVVM-Android-OMQA/#) - Builds & uploads app from develop branch in uat environment.

## APIs:

We are using [StarWars Api](https://swapi.co/) to get list of StarWars characters.

