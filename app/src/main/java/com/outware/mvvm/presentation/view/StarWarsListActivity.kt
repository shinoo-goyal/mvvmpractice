package com.outware.mvvm.presentation.view

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.jakewharton.rxbinding2.widget.RxTextView
import com.outware.mvvm.R
import com.outware.mvvm.presentation.viewmodel.StarWarsListViewModel
import io.reactivex.Observable
import kotlinx.android.synthetic.main.activity_main.*

/**
 * @author shinoo.goyal
 */
class StarWarsListActivity : BaseActivity() {

    private var listAdapter = StarWarsAdapter()
    private lateinit var viewModel: StarWarsListViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injectActivity()
        setContentView(R.layout.activity_main)
        setupObservers()
        getCharacterList()
    }

    override fun injectActivity() = getAppInjector().inject(this)

    private fun setupObservers() {
        viewModel = ViewModelProviders.of(this).get(StarWarsListViewModel::class.java)
        viewModel.starwarsData.observe(this, Observer {
            it?.let {
                listAdapter.setStarWarsList(it.characters)
                edt_search.isEnabled = true
            }
        })

        viewModel.errorData.observe(this, Observer {
            it?.let {
                handleError(it.message)
            }
        })

        list_characters.layoutManager = LinearLayoutManager(this)
        list_characters.adapter = listAdapter

        val searchObservable: Observable<CharSequence> = RxTextView.textChanges(edt_search)
        subscriptions.add(searchObservable.subscribe {
            listAdapter.setStarWarsList(getFilteredList(it.toString()))
        })
    }

    private fun getFilteredList(searchText: String) =
            viewModel.starwarsData.value?.characters?.let {
                it.filter { it.name.contains(searchText, true) }
            } ?: emptyList()


    private fun getCharacterList() = viewModel.getCharacterList()

    private fun handleError(message: String?) {
        edt_search.isEnabled = false
        val alertDialogBuilder = android.support.v7.app.AlertDialog.Builder(this)
                .setCancelable(false)
                .setTitle(getString(R.string.error_message))
                .setMessage(message ?: "")
                .setPositiveButton(getString(R.string.ok), { _, _ -> getCharacterList() })
        alertDialogBuilder.show()
    }

}
