package com.outware.mvvm.presentation.view

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.outware.mvvm.R
import com.outware.mvvm.domain.models.entities.StarWarsCharacter
import kotlinx.android.synthetic.main.layout_star_wars_item.view.*

class StarWarsAdapter : RecyclerView.Adapter<StarWarsAdapter.StarWarsItemViewHolder>() {

    private var starWarsList: List<StarWarsCharacter> = emptyList()

    override fun getItemCount() = starWarsList.size

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): StarWarsItemViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.layout_star_wars_item, parent, false)
        return StarWarsItemViewHolder(view)
    }

    fun setStarWarsList(list: List<StarWarsCharacter>) {
        this.starWarsList = list
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: StarWarsItemViewHolder, position: Int) {
        holder.bindData(starWarsList[position])
    }

    class StarWarsItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindData(starWarsCharacter: StarWarsCharacter) {
            itemView.txt_name.text = starWarsCharacter.name
            //itemView.setOnClickListener { listener(starWarsCharacter) }
        }
    }
}

