package com.outware.mvvm.presentation.view

import android.os.Bundle
import android.os.PersistableBundle
import android.support.v7.app.AppCompatActivity
import com.outware.mvvm.DataBindingApplication
import com.outware.mvvm.inection.components.DaggerActivityComponent
import io.reactivex.disposables.CompositeDisposable

abstract class BaseActivity : AppCompatActivity() {

    val subscriptions = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)
        injectActivity()
    }

    private fun getAppComponent() = (applicationContext as DataBindingApplication).applicationComponent

    fun getAppInjector() = DaggerActivityComponent.builder().applicationComponent(getAppComponent()).build()

    abstract fun injectActivity()

    override fun onDestroy() {
        super.onDestroy()
        subscriptions.dispose()
    }
}