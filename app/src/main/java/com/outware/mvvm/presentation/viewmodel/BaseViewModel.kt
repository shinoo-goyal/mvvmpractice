package com.outware.mvvm.presentation.viewmodel

import android.arch.lifecycle.ViewModel
import com.outware.mvvm.inection.components.DaggerViewModelComponent
import com.outware.mvvm.util.Schedulers
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

abstract class BaseViewModel : ViewModel() {

    @Inject
    lateinit var schedulers: Schedulers

    val compositeDisposable = CompositeDisposable()

    init {
        inject()
    }

    abstract fun inject()

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }

    fun getViewModelComponent() = DaggerViewModelComponent.builder().build()
}