package com.outware.mvvm.presentation.viewmodel

import android.arch.lifecycle.MutableLiveData
import com.outware.mvvm.domain.models.entities.StarWarsResponseWrapper
import com.outware.mvvm.domain.usecases.GetStarWarsCharactersUseCase
import javax.inject.Inject

class StarWarsListViewModel : BaseViewModel() {

    @Inject
    lateinit var getStarWarsCharactersUseCase: GetStarWarsCharactersUseCase
    val starwarsData = MutableLiveData<StarWarsResponseWrapper>()
    val errorData = MutableLiveData<Exception>()

    override fun inject() = getViewModelComponent().inject(this)

    fun getCharacterList() {
        compositeDisposable.add(getStarWarsCharactersUseCase.execute()
                .observeOn(schedulers.ui())
                .subscribe({
                    starwarsData.postValue(it)
                }, {
                    errorData.postValue(Exception(it.message))
                }))
    }
}