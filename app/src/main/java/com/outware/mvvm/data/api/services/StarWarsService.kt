package com.outware.mvvm.data.api.services

import com.outware.mvvm.data.api.models.StarWarsResponse
import retrofit2.Call
import retrofit2.http.GET

interface StarWarsService {
    @GET("people")
    fun getStarWarsCharacterList() : Call<StarWarsResponse>
}