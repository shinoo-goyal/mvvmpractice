package com.outware.mvvm.data.api

import retrofit2.Call
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CallCoordinator @Inject constructor() {
    /**
     * Synchronously executes the provided call and, if successful, returns the parsed response
     * body. If the call is not successful an attempt is made to parse the response error body.
     */
    fun <T> execute(call: Call<T>): T {
        try {
            val response = call.execute()

            if (response.isSuccessful) {
                return response.body()
            } else {
                throw Exception()
            }
        } catch (exception: Exception) {
            throw exception
        }
    }

}

