package com.outware.mvvm.data.managers

import au.com.starwars.domain.repositories.StarWarsRepository
import com.outware.mvvm.data.api.CallCoordinator
import com.outware.mvvm.data.api.services.StarWarsService
import com.outware.mvvm.data.mappers.StarWarsMapper
import com.outware.mvvm.domain.models.entities.StarWarsResponseWrapper
import io.reactivex.Single
import javax.inject.Inject

class StarWarsManager @Inject constructor(private val starWarsService: StarWarsService,
                                          private val callCoordinator: CallCoordinator) : StarWarsRepository {

    override fun getStarWarsCharacterList(): Single<StarWarsResponseWrapper> {
        return Single.fromCallable {
            val response = callCoordinator.execute(starWarsService.getStarWarsCharacterList())
            StarWarsMapper.mapToStarWarsMapper(response.results)
        }
    }
}