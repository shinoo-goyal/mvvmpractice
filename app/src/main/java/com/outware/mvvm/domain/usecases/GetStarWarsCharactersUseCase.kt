package com.outware.mvvm.domain.usecases

import au.com.starwars.domain.repositories.StarWarsRepository
import com.outware.mvvm.util.Schedulers
import javax.inject.Inject

class GetStarWarsCharactersUseCase @Inject constructor(private val starWarsRepository: StarWarsRepository,
                                                       private val schedulers: Schedulers) {

    fun execute() = starWarsRepository.getStarWarsCharacterList().subscribeOn(schedulers.io())
}