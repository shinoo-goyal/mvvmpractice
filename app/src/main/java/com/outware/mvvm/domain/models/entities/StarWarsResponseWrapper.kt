package com.outware.mvvm.domain.models.entities

class StarWarsResponseWrapper(val characters: List<StarWarsCharacter>)

class StarWarsCharacter(val name: String,
                        val birthYear: String,
                        val species: String)
