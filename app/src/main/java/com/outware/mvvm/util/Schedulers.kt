package com.outware.mvvm.util

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers.from
import java.util.concurrent.Executors
import java.util.concurrent.ThreadFactory
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
open class Schedulers @Inject constructor() {

    open fun ui(): Scheduler {
        return AndroidSchedulers.mainThread()
    }

    open fun io(): Scheduler {
        return io.reactivex.schedulers.Schedulers.io()
    }
}