package com.outware.mvvm

import android.app.Application
import com.outware.mvvm.inection.components.ApplicationComponent
import com.outware.mvvm.inection.components.DaggerApplicationComponent
import javax.inject.Inject

class DataBindingApplication : Application() {

    @Inject
    lateinit var applicationComponent: ApplicationComponent

    override fun onCreate() {
        super.onCreate()
        inject()
    }

    fun inject(){
        DaggerApplicationComponent.builder().build().inject(this)
    }
}