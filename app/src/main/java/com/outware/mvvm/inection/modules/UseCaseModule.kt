package com.outware.mvvm.inection.modules

import com.outware.mvvm.domain.usecases.GetStarWarsCharactersUseCase
import dagger.Module
import dagger.Provides

@Module
interface UseCaseModule {
    fun bindGetStarWarsListUseCase(): GetStarWarsCharactersUseCase
}