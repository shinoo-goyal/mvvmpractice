package com.outware.mvvm.inection.components

import com.outware.mvvm.inection.modules.NetworkModule
import com.outware.mvvm.inection.modules.RepositoryModule
import com.outware.mvvm.inection.modules.UseCaseModule
import com.outware.mvvm.presentation.viewmodel.StarWarsListViewModel
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [UseCaseModule::class, RepositoryModule::class, NetworkModule::class])
interface ViewModelComponent : RepositoryComponent{
    fun inject(starWarsListViewModel: StarWarsListViewModel)
}