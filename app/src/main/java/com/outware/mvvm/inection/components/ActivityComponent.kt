package com.outware.mvvm.inection.components

import com.outware.mvvm.inection.annotations.PerScreen
import com.outware.mvvm.presentation.view.StarWarsListActivity
import dagger.Component

@PerScreen
@Component(dependencies = [ApplicationComponent::class])
interface ActivityComponent {
    fun inject(activity: StarWarsListActivity)
}