package com.outware.mvvm.inection.components

import au.com.starwars.domain.repositories.StarWarsRepository

interface RepositoryComponent {
    fun provideStarWarsRepository(): StarWarsRepository
}