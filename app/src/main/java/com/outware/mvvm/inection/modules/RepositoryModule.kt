package com.outware.mvvm.inection.modules

import au.com.starwars.domain.repositories.StarWarsRepository
import com.outware.mvvm.data.managers.StarWarsManager
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
interface RepositoryModule {
    @Binds
    @Singleton
    fun bindStarWarsRepository(starWarsManager: StarWarsManager): StarWarsRepository
}