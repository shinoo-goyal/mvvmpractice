package com.outware.mvvm.inection.annotations

import javax.inject.Scope

@Scope
@kotlin.annotation.Retention(kotlin.annotation.AnnotationRetention.RUNTIME)
annotation class PerScreen
