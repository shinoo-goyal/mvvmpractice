package com.outware.mvvm.inection.modules

import com.outware.mvvm.data.api.services.StarWarsService
import dagger.Module
import dagger.Provides
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton
import  com.outware.mvvm.Configuration

@Module
class NetworkModule {

    @Singleton
    @Provides
    fun provideRetrofit(configuration: Configuration) = buildRetrofit(configuration)

    @Singleton
    @Provides
    fun provideConfiguration() = Configuration()

    private fun buildRetrofit(configuration: Configuration) = Retrofit.Builder().baseUrl(configuration.baseUrl)
            .client(OkHttpClient.Builder().build())
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .build()

    @Singleton
    @Provides
    fun provideStarWarsService(retrofit: Retrofit) = retrofit.create(StarWarsService::class.java)
}