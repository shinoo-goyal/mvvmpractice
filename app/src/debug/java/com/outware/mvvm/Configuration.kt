package com.outware.mvvm

import javax.inject.Inject

class Configuration @Inject constructor() : ConfigurationImpl {
    override val baseUrl = "https://swapi.co/api/"
}