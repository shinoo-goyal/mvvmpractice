package com.outware.mvvm

import javax.inject.Inject

class Configuration @Inject constructor() {
    val baseUrl = "https://swapi.co/api/"
}