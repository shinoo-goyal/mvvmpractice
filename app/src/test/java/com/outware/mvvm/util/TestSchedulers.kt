package com.outware.mvvm.util

import io.reactivex.Scheduler

class TestSchedulers : Schedulers() {

    override fun ui(): Scheduler {
        return io.reactivex.schedulers.Schedulers.trampoline()
    }

    override fun io(): Scheduler {
        return io.reactivex.schedulers.Schedulers.trampoline()
    }
}