package com.outware.mvvm.presentation.viewmodel

import android.arch.core.executor.testing.InstantTaskExecutorRule
import com.outware.mvvm.domain.models.entities.StarWarsCharacter
import com.outware.mvvm.domain.models.entities.StarWarsResponseWrapper
import com.outware.mvvm.domain.usecases.GetStarWarsCharactersUseCase
import com.outware.mvvm.util.RxSchedulersOverrideRule
import com.outware.mvvm.util.TestSchedulers
import io.reactivex.Single
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

/**
 * @author shinoo.goyal
 */
class StarWarsListViewModelTest {

    @Rule
    @JvmField
    val rxSchedulersOverrideRule: RxSchedulersOverrideRule = RxSchedulersOverrideRule()

    @Mock
    lateinit var getStarWarsCharactersUseCase: GetStarWarsCharactersUseCase
    val viewModel = StarWarsListViewModel()

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        viewModel.schedulers = TestSchedulers()
        viewModel.getStarWarsCharactersUseCase = getStarWarsCharactersUseCase
    }

    @Rule
    @JvmField
    val instantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    @Test
    fun getCharacterList_onSuccess_setDataValue() {
        // Arrange
        val starWarsResponseWrapper = StarWarsResponseWrapper(
                listOf(StarWarsCharacter("a", "b", "c")))
        Mockito.`when`(getStarWarsCharactersUseCase.execute()).thenReturn(Single.just(starWarsResponseWrapper))

        // Act
        viewModel.getCharacterList()

        // Assert
        Assert.assertEquals(starWarsResponseWrapper, viewModel.starwarsData.value)
    }

    @Test
    fun getCharacterList_onFailure_throwError() {
        // Arrange
        val exception = Exception("Exception")
        Mockito.`when`(getStarWarsCharactersUseCase.execute()).thenReturn(Single.error(exception))

        // Act
        viewModel.getCharacterList()

        // Assert
        Assert.assertEquals(exception.message, viewModel.errorData.value?.message)
    }
}