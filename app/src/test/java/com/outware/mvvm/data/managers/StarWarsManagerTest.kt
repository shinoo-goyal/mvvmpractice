package com.outware.mvvm.data.managers

import android.arch.core.executor.testing.InstantTaskExecutorRule
import au.com.starwars.domain.repositories.StarWarsRepository
import com.outware.mvvm.data.api.CallCoordinator
import com.outware.mvvm.data.api.models.StarWarsResponse
import com.outware.mvvm.data.api.models.StarWarsResult
import com.outware.mvvm.data.api.services.StarWarsService
import com.outware.mvvm.domain.models.entities.StarWarsCharacter
import com.outware.mvvm.domain.models.entities.StarWarsResponseWrapper
import com.outware.mvvm.util.RxSchedulersOverrideRule
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

/**
 * @author shinoo.goyal
 */
class StarWarsManagerTest {

    @Rule
    @JvmField
    val rxSchedulersOverrideRule: RxSchedulersOverrideRule = RxSchedulersOverrideRule()

    @Mock
    lateinit var starWarsService: StarWarsService

    @Mock
    lateinit var callCoordinator: CallCoordinator

    @Mock
    lateinit var call: retrofit2.Call<StarWarsResponse>

    lateinit var starWarsRepository: StarWarsRepository

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        Mockito.`when`(starWarsService.getStarWarsCharacterList()).thenReturn(call)
        starWarsRepository = StarWarsManager(starWarsService, callCoordinator)
    }

    @Rule
    @JvmField
    val instantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    @Test
    fun getCharacterList_onSuccess_returnMappedResponse() {
        // Arrange
        val characterList = listOf(StarWarsResult("a", "b", listOf("c")),
                StarWarsResult("d", "e", listOf("f")),
                StarWarsResult("g", "h", listOf("i")))
        val starWarsResponse = StarWarsResponse(characterList)

        Mockito.`when`(callCoordinator.execute(call)).thenReturn(starWarsResponse)

        // Act
        val testObserver = starWarsRepository.getStarWarsCharacterList().test()
        val responseWrapper = StarWarsResponseWrapper(listOf(StarWarsCharacter("a", "b", "c"),
                StarWarsCharacter("d", "e", "f"),
                StarWarsCharacter("g", "h", "i")))

        // Assert
        testObserver.assertComplete()
        testObserver.assertNoErrors()
        Assert.assertEquals(characterList.size, responseWrapper.characters.size)
    }

    @Test(expected = Exception::class)
    fun getCharacterList_onFailure_throwError() {
        // Arrange
        val exception = Exception("Exception")
        Mockito.`when`(callCoordinator.execute(call)).thenThrow(exception)

        // Act
        val testObserver = starWarsRepository.getStarWarsCharacterList().test()

        // Assert
        testObserver.assertNotComplete()
    }
}