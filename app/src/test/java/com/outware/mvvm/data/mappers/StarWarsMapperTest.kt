package com.outware.mvvm.data.mappers

import com.outware.mvvm.data.api.models.StarWarsResult
import org.junit.Assert
import org.junit.Test

class StarWarsMapperTest {

    @Test
    fun getCharacterList_onValidList_returnMappedResponse() {
        val characterList = listOf(StarWarsResult("a", "b", listOf("c")),
                StarWarsResult("d", "e", listOf("f")),
                StarWarsResult("g", "h", listOf("i")))
        val mappedList = StarWarsMapper.mapToStarWarsMapper(characterList)

        Assert.assertEquals(mappedList.characters.size, characterList.size)
        Assert.assertEquals(mappedList.characters[0].birthYear, characterList[0].birthYear)
        Assert.assertEquals(mappedList.characters[0].species, characterList[0].species[0])
    }

    @Test
    fun getCharacterList_onEmptyList_returnMappedResponse() {
        val mappedList = StarWarsMapper.mapToStarWarsMapper(null)
        Assert.assertEquals(mappedList.characters.size, 0)
    }
}